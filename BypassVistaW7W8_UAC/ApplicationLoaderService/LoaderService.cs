﻿using System;
using System.ServiceProcess;
using System.IO;

namespace Toolkit
{
    public partial class LoaderService : ServiceBase
    {
        public LoaderService()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            String applicationName = "";
            // the name of the application to launch;
            // to launch an application using the full command path simply escape
            // the path with quotes, for example to launch firefox.exe:
            
            //applicationName = "\"C:\\Program Files (x86)\\Mozilla Firefox\\firefox.exe\"";
            //applicationName = "cmd.exe";
            
            String filename = @"C:\Users\Public\Documents\appToLoad.txt";
                  using (StreamReader sr = new StreamReader(filename))
                  {
                      applicationName = sr.ReadToEnd();
                      //Console.WriteLine(line);
                  }
            // launch the application
            ApplicationLoader.PROCESS_INFORMATION procInfo;
            ApplicationLoader.StartProcessAndBypassUAC(applicationName, out procInfo);
        }

        protected override void OnStop()
        {
        }
    }
}
